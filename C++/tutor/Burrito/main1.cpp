#include <iostream>
#include <string>
using namespace std;

class Human
{
private: 
    string name; 
    int age;
  
public:
  Human(string humansName, int humansAge) // only constructor in class 
// Human ( )
  {
    name = humansName;
    age = humansAge;
    cout << "Overloaded constructor creates" << name;
    
    cout << "of age"<< age << endl;
   
  }

void IntroducesSelf(){
  
  cout << "I am " + name << " and am ";
  cout << age << " years old" << endl;
  
  
}



};


int main()
{
 //  instantiate two objects of class Human
Human firstMan("Adam", 25);
Human firstWoman("Eve", 28);
  
firstWoman.IntroducesSelf();
firstMan.IntroducesSelf();
}